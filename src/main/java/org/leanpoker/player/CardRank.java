package org.leanpoker.player;

import static java.util.Arrays.stream;

/**
 * Created by Bence_Varga on 7/29/2016.
 */
public class CardRank {
    private String[] cards = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

    public int getRank(String card) {
        for (int i = 0; i < cards.length; i++) {
            if (cards[i].equals(card))
                return i;
        }
        return -1;
    }
}
