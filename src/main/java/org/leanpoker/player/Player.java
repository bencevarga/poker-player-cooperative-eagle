package org.leanpoker.player;

import com.google.gson.*;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class Player {

    static final String VERSION = "Default Java folding player";
    private static final CardRank cardRank = new CardRank();

    public static int betRequest(JsonElement request) {
        JsonObject gameState = request.getAsJsonObject();
        int in_action = gameState.getAsJsonPrimitive("in_action").getAsInt();

        JsonObject player = gameState.getAsJsonArray("players").get(in_action).getAsJsonObject();
        JsonArray holeCards =  player.getAsJsonArray("hole_cards");
        JsonArray communityCards = gameState.getAsJsonArray("community_cards");

        int bet = 20;

        if (communityCards.size() == 0) {
            bet *= preFlopVote(holeCards);
        } else if (communityCards.size() == 3) {
            bet *= postFlopVote(holeCards, communityCards);
        } else if (communityCards.size() == 4) {
            bet *= postFlopVote(holeCards, communityCards);
        } else if (communityCards.size() == 5) {
            bet *= postFlopVote(holeCards, communityCards);
        }

        return bet;
    }

    private static double postFlopVote(JsonArray holeCards, JsonArray communityCards) {
        JsonObject[] result = new JsonObject[5];

        for (int i = 0; i < holeCards.size(); i++) {
            result[i] = holeCards.get(i).getAsJsonObject();

        }
        for (int i = 2; i < communityCards.size(); i++) {
            result[i] = communityCards.get(i).getAsJsonObject();
        }

        for (int i = 0; i < result.length; i++) {
            JsonObject jsonObject = result[i];
            System.err.println("RESULT: " + jsonObject);
        }

        String response = given().param("cards", result).when().get("http://rainman.leanpoker.org/rank").then().extract().response().asString();
        System.err.println("RESPONSE: " + response);
//        JsonParser parser = new JsonParser();
//        JsonElement jsonTree = parser.parse(response);
//
//        int rank = jsonTree.getAsJsonObject().getAsJsonObject("rank").getAsInt();

//        return (rank * rank) / 100 + 1.0;
        return 1.1;
    }

    private static double preFlopVote(JsonArray holeCards) {
        if (getRank(holeCards, 0).equals(getRank(holeCards, 1))){
            if (cardRank.getRank(getRank(holeCards, 0)) > 8) {
                return 1.5;
            }
            else {
                return 1.2;
            }
        }

        return 0.8;
    }

    private static String getRank(JsonArray holeCards, int index) {
        return holeCards.get(index).getAsJsonObject().getAsJsonPrimitive("rank").getAsString();
    }

    public static void showdown(JsonElement game) {
    }
}
